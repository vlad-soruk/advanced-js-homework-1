// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), 
// age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися 
// під час створення об'єкта.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    // Створіть гетери та сеттери для цих властивостей
    get name(){
        return `Name of the employee: ${this._name}`;
    }

    set name(value){
        if(value.length < 3){
            console.log(`Name should have 3 or more symbols! "${value}" should be longer.`);
        }
        else{
            this._name = value
        }
    }

    get age(){
        return `Age of the employee: ${this._age} years old`;
    }

    set age(value){
        if(value < 1 || value > 100){
            console.log(`Your age is inappropriate. Please, try again!`);
        }
        else{
            this._age = value
        }
    }

    get salary(){
        return `Salary of the employee: ${this._salary}`;
    }

    set salary(value){
        if(value < 6700 || value > 1000000){
            console.log(`Your salary cannot be less than 6700 or more than 1 million. Please, try again!`);
        }
        else{
            this._salary = value
        }
    }
}

let employee = new Employee("Vlad", 20, 60000);
console.log(employee);

// Клас Programmer, який успадковуватиметься від класу Employee, 
// і який матиме властивість lang
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);

        this._lang = lang
    }

    // Перезапишіть гетер для властивості salary. 
    // Нехай він повертає властивість salary, помножену на 3
    get salary(){
        return `Your salary multiplied by 3 = ${this._salary} * 3 = ${this._salary * 3}`; 
    }
    set salary(value){
        if(value < 6700 || value > 1000000){
            console.log(`Your salary cannot be less than 6700 or more than 1 million. Please, try again!`);
        }
        else{
            this._salary = value
        }
    }
} 

// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль
let programmer1 = new Programmer("Maksim", 25, 100000, ['javascript', 'Ruby', 'Python']);
console.log(programmer1);
let programmer2 = new Programmer("Oleg", 35, 140000, ['javascript', 'Ruby', 'Python', 'Scala']);
console.log(programmer2);
let programmer3 = new Programmer("Oleksandr", 29, 60000, ['javascript', 'Python']);
console.log(programmer3);
let programmer4 = new Programmer("Olga", 38, 180000, ['javascript', 'Typescript', 'Python', 'PHP']);
console.log(programmer4);
let programmer5 = new Programmer("Roma", 55, 210000, ['javascript', 'PHP', 'Python', 'C++', 'C#', 'Typescript']);
console.log(programmer5);